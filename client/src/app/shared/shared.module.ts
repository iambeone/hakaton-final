import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '@app/material.module';
import { GeneralLayoutComponent } from "./layouts/general/general.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HeaderComponent } from "./components/header/header.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { YesnoModalComponent } from './modals/yesno-modal/yesno-modal.component';

const
    angularModules = [
        CommonModule,
        HttpClientModule,
        RouterModule,
        FormsModule, 
        ReactiveFormsModule
    ],
    extModules = [
        MaterialModule
    ],
    components = [
        FooterComponent,
        HeaderComponent,
        YesnoModalComponent
    ],
    layouts = [
        GeneralLayoutComponent
    ]

@NgModule({
    imports : [...angularModules, ...extModules],
    declarations: [...components, ...layouts, YesnoModalComponent],
    exports: [...angularModules],
    entryComponents: [YesnoModalComponent]
})
export class SharedModule { }
