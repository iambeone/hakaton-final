import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { environment } from '@env/environment';
import {
  HttpClient,
  HttpEvent,
  HttpHeaders,
  HttpEventType,
  HttpParams,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {
  apiUrl: string  = environment.apiUrl;


  constructor(private http: HttpClient) { }

  get<TResponse, TRequest>(url: string, params?: TRequest): Observable<TResponse> {
    const queryParams = this.convertModelToQueryParams(params);
    console.log(this.apiUrl + `/${url}`)
    return this.http.get<TResponse>(this.apiUrl + `/${url}`, {params: queryParams})
  }

  post<TResponse, TRequest>(url: string, params?: TRequest): Observable<TResponse> {
    return this.http.post<TResponse>(this.apiUrl + `/${url}`, params)
  }

  delete<TResponse, TRequest>(url: string, params?: TRequest): Observable<TResponse> {
    return this.http.delete<TResponse>(this.apiUrl + `/${url}`, params)
  }

  put<TResponse, TRequest>(url: string, params?: TRequest): Observable<TResponse> {
    return this.http.put<TResponse>(this.apiUrl + `/${url}`, params)
  }

  private convertModelToQueryParams<TModel>(model: TModel): HttpParams {
      let queryParams = new HttpParams();
      if (!model) {
          return queryParams;
      }
      Object.keys(model).forEach(p => {
          queryParams = queryParams.set(p, model[p]);
      });
      return queryParams;
  }

}
