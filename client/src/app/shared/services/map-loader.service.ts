import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

declare var ymaps:any;

@Injectable({
  providedIn: 'root'
})
export class MapLoaderService {
  map$: Subject<any>;

  constructor() {
    this.map$ = new Subject<any>();
  }

  getMap(element: string, coords: number[]): Observable<any> {
    this.map$.next(null)
    const scriptYmaps = document.createElement('script');
    scriptYmaps.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=985a5b9b-d668-4343-9097-2696ce2628a0';
    document.body.appendChild(scriptYmaps);
    const createMap = () => {
    return  new ymaps.Map(
        element,
        {
        center: coords,
        zoom: 18
        }
    )
    }
    scriptYmaps.onload = () => {
        ymaps.ready().then(() => {
            this.map$.next(createMap())
        })
    }
    return this.map$;
  }
}