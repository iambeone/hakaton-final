import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-yesno-modal',
  templateUrl: './yesno-modal.component.html',
  styleUrls: ['./yesno-modal.component.scss']
})
export class YesnoModalComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<Inject>) { }

  ngOnInit() {
  }

  Yes(result:any) {
    this.dialogRef.close(true);
  }

  No(result:any) {
    this.dialogRef.close(false);
  }
}
