import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralLayoutComponent } from './shared/layouts/general/general.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralLayoutComponent,
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            redirectTo: 'home',
            pathMatch: 'full'
          },          
          {
            path: 'home',
            loadChildren: './modules/home/home.module#HomeModule',
          },
          {
            path: 'pointtype',
            loadChildren: './modules/pointtype/pointtype.module#PointTypeModule',
          },
          {
            path: 'owner',
            loadChildren: './modules/owner/owner.module#OwnerModule',
          },
          {
            path: 'point',
            loadChildren: './modules/point/point.module#PointModule',
          },
          {
            path: 'sensor',
            loadChildren: './modules/sensor/sensor.module#SensorModule',
          },
          {
            path: 'pipe',
            loadChildren: './modules/pipe/pipe.module#PipeModule',
          },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
