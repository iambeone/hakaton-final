import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { HomeRouting } from './home.routing';
import { SharedModule } from '@app/shared/shared.module';
import { GeneralHomeLayoutComponent } from './layouts/general-home-layout/general-home-layout.component';
import { MaterialModule } from '@app/material.module';

@NgModule({
  declarations: [HomepageComponent, GeneralHomeLayoutComponent],
  imports: [
    HomeRouting,
    CommonModule,
    SharedModule,
    MaterialModule
  ],
})
export class HomeModule { }
