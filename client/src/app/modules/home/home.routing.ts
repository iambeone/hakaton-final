import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { GeneralHomeLayoutComponent } from './layouts/general-home-layout/general-home-layout.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralHomeLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full', 
        component: HomepageComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRouting { }