import { Component, OnInit } from '@angular/core';
import { PointService } from '@app/modules/point/services/point.service';
import { SensorService } from '@app/modules/sensor/services/sensor.service';
import { PipeService } from "@app/modules/pipe/services/pipe.service";
import { Observable } from 'rxjs';

declare var ymaps:any;
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  public map :any;
  pipeCollection
  sensorCollection
  pointCollection
  showPipes:boolean = true;
  showPoints:boolean = false;
  showSensors:boolean = false;
  showSensorInterval

  constructor(private pointService:PointService, private sensorService:SensorService, private pipeService:PipeService) { }

  _showPipes(){
    if(!this.showPipes){
      this.pipeCollection.removeAll()
    }else{
      this.pipeService.load()
    }
  }
  _showPoints(){
    if(!this.showPoints){
      this.pointCollection.removeAll()
    }else{
      this.pointService.load()
    }
  }
  _showSensors(){
    if(!this.showSensors){
      this.sensorCollection.removeAll()
      clearInterval(this.showSensorInterval)
    }else{
      this.sensorService.load()
      this.showSensorInterval = setInterval(() => this.sensorService.load(), 5000);
    }
  }

  ngOnInit() {
    ymaps.ready().then(() => {
      this.map = new ymaps.Map('map', {
        center: [55.797224, 49.108215],
        zoom: 15
      });
      this.sensorCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'islands#circleIcon'
      });
      this.pipeCollection = new ymaps.GeoObjectCollection();
      this.pointCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'islands#governmentCircleIcon'
      });
      this.pipeService.items$.subscribe(data => {
        if(data){
          data.map(pipe => {
            const coords = pipe.points.map(point => {
              return [point.lat, point.lng]
            })
            let lineColor = '#000000';
            switch(pipe.type){
              case 'water':
                  lineColor = '#0B9CC9';
                  break;
              case 'heat':
                  lineColor = '#BC2A1F';
                  break;
              case 'sewer':
                  lineColor = '#5DB100';
                  break;
              case 'gas':
                  lineColor = '#C09B1C';
                  break;
            }
            let myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "LineString",
                    coordinates: coords
                },
                // Свойства.
                properties: {
                    // Контент метки.
                    iconContent: pipe.title,
                    hintContent: pipe.title
                }
            }, {
              // Задаем опции геообъекта.
              // Включаем возможность перетаскивания ломаной.
              draggable: false,
              // Цвет линии.
              strokeColor: lineColor,
              // Ширина линии.
              strokeWidth: 5
            });
            //this.map.geoObjects.add(myGeoObject)
            this.pipeCollection.add(myGeoObject)
          })
          this.map.geoObjects.add(this.pipeCollection)
        }
      })
      this.pointService.items$.subscribe(data => {
        if(data){
          data.map(point => {
            let color = '#000000';
            switch(point['type']['id']){
              case 1:
                  color = '#9B42DA';
                  break;
              case 2:
                  color = '#42DA54';
                  break;
              case 3:
                  color = '#42CEDA';
                  break;
              case 4:
                  color = '#DA4242';
                  break;
            }      
            let myGeoObject = new ymaps.GeoObject({
              // Описание геометрии.
              geometry: {
                  type: "Point",
                  coordinates: [point.lat, point.lng]
              },
              // Свойства.
              properties: {
                  // Контент метки.
                  iconContent: point.title,
                  hintContent: point['type']['title']
              }
          }, {
            preset: 'islands#governmentCircleIcon',
            iconColor: color
            })
            //this.map.geoObjects.add(myGeoObject)
            this.pointCollection.add(myGeoObject)
          })
          this.map.geoObjects.add(this.pointCollection)
        }
      })
      this.sensorService.items$.subscribe(data => {
        if(data){
          this.sensorCollection.removeAll();
          data.map(point => {
            let color = '#52E20F'
            if(point.value >= point.warn){
              color = '#E2DC0F'
            }
            if(point.value >= point.risk){
              color = '#E2340F'
            }
            let myGeoObject = new ymaps.GeoObject({
              // Описание геометрии.
              geometry: {
                  type: "Point",
                  coordinates: [point.lat, point.lng]
              },
              // Свойства.
              properties: {
                  // Контент метки.
                  hintContent: point['type']['title']
              }
              }, {
                preset: 'islands#CircleDotIcon',
                iconColor: color
              })
              this.sensorCollection.add(myGeoObject)
          })
          this.map.geoObjects.add(this.sensorCollection)
        }
      })
      if(this.showPipes){
        this.pipeService.load();
      }
      if(this.showPoints){
        this.pointService.load();
      }      
      if(this.showSensors){
        this.showSensorInterval = setInterval(() => this.sensorService.load(), 5000);
      }
      
    });
  }

}
