export interface Pipe {
    id: number
    title: string
    lng: string
    lat: string
    type: string
    comment?: string
    constYear? :string
    depreciation? :string
    constType? :string
    points?: {lat:string, lng:string}[]
}
