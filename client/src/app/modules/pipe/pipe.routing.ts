import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralPipeLayoutComponent } from './layouts/general-pipe-layout/general-pipe-layout.component';
import { PipePageComponent } from './pages/pipe-page/pipe-page.component';


const routes: Routes = [
  {
    path: '',
    component: GeneralPipeLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full', 
        component: PipePageComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PipeRouting { }