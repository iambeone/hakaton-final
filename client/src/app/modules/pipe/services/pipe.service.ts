import { Injectable } from '@angular/core';
import { BaseApiService } from '@app/shared/services/base-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pipe } from "../models/pipe.model";

@Injectable({
  providedIn: 'root'
})
export class PipeService {
  private _items = new BehaviorSubject<Pipe[]>([]);

  constructor(private api:BaseApiService) {
    
  }

  get items$(): BehaviorSubject<Pipe[]>{
    return this._items;
  }

  load(filter?:[], all?:boolean){
    if(all){
      filter['all'] = true;
    }
    this.api.get('pipe', filter).subscribe((data:Pipe[]) => {
        this._items.next(data);
    })
  }

  add(item:Pipe){
    return this.api.post('pipe', item).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }

  delete(id:number){
    return this.api.delete('pipe/'+id).
      pipe(map(data => {
        this._items.next(this._items.value.filter(item => item.id != id))
      }))
  }

  edit(id:number, payload:Pipe){
    return this.api.put('pipe/' + id, payload).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }
}
