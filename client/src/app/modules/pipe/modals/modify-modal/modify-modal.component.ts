import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Pipe } from '../../models/pipe.model';
import { PipeService } from '../../services/pipe.service';
import { OwnerService } from '@app/modules/owner/services/owner.service';
import { Owner } from '@app/modules/owner/models/owner.model';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-modify-modal',
  templateUrl: './modify-modal.component.html',
  styleUrls: ['./modify-modal.component.scss']
})
export class ModifyModalComponent implements OnInit {
  isnew:boolean = true;
  id:number;
  Pipe: string;
  errors:string[] = [];
  owners: Owner[] = [];
  fields:any[] = [];
  types = [
    { key: "water", title: 'Водоснабжения' },
    { key: "heat", title: 'Теплоснабжения' },
    { key: "gas", title: 'Газовая' },
    { key: "sewer", title: 'Канализационная' },
  ]
  constTypes = [
    { key: "Castiron", title: 'Чугунная' },
    { key: "Steel", title: 'Стальная' },
    { key: "Asbestos", title: 'Асбестоцементные' },
    { key: "ReinforcedConcrete", title: 'Железобетон' },
  ]


  modifyForm = new FormGroup({
    title: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    owner: new FormControl('', Validators.required),
    comment: new FormControl('', Validators.required),    
    constYear: new FormControl('', Validators.required),
    depreciation: new FormControl('', Validators.required),
    constType: new FormControl('', Validators.required),
    coords: new FormControl(''),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {action: 'edit'|'add', data?: Pipe, Pipe?:string, coords:string}, private dialogRef: MatDialogRef<Inject>, private PipeService: PipeService, private ownerService:OwnerService) { }

  ngOnInit() {
    if(this.data && this.data.action){
      this.isnew = this.data.action == 'add';
      this.Pipe = this.data.Pipe;
      this.modifyForm.controls.coords.setValue(this.data.coords);
      if(this.data.data && this.data.action == 'edit'){
        // assignin values
        this.modifyForm.controls.title.setValue(this.data.data.title);
        this.id = this.data.data.id;
      }
      if(!this.owners.length){
        this.ownerService.items$.subscribe( (items:Owner[]) => {
          this.owners = items;
        })
        this.ownerService.load();
      }

    }
  }

  async onSubmit(){
    // sending values
    if(this.isnew){
      this.PipeService.add(this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }else{
      this.PipeService.edit(this.id, this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }
  }

  Close(result:any) {
    this.dialogRef.close(result);
  }
}
