import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Pipe } from "../../models/pipe.model";
import { PipeService } from "../../services/pipe.service";
import { PointService } from '@app/modules/point/services/point.service';
import { ModifyModalComponent } from "../../modals/modify-modal/modify-modal.component";
import { MatDialog } from '@angular/material';

declare var ymaps:any;

@Component({
  selector: 'app-pipe-page',
  templateUrl: './pipe-page.component.html',
  styleUrls: ['./pipe-page.component.scss']
})
export class PipePageComponent implements OnInit {
  public map :any;
  items: Pipe[];
  disabled:boolean = true;
  title: string = 'Трубы';
  coords: [any, any][] = [];
  notification:{
    notice?: string,
    type?: 'danger' | 'success',
    show:boolean
  } = {show:false}


  constructor(private service:PipeService, public dialog: MatDialog, private pointService:PointService, private ChangeDetectorRef:ChangeDetectorRef) { }

  ngOnInit() {
    ymaps.ready().then(() => {
      this.map = new ymaps.Map('map', {
        center: [55.797224, 49.108215],
        zoom: 18
      });
      this.map.events.add('click', e => {
        var coords = e.get('coords');
        this.coords.push([coords[0].toPrecision(8), coords[1].toPrecision(8)])
        if(this.coords.length > 1){
          this.disabled = false;
          this.ChangeDetectorRef.detectChanges()
          var myGeoObject = new ymaps.GeoObject({
              geometry: {
                  type: "LineString",
                  coordinates: this.coords
              },
          }, {
              draggable: false,
              strokeColor: "#000000",
              strokeWidth: 5
          });
          this.map.geoObjects.add(myGeoObject);
        }
      })
      this.service.items$.subscribe(data => {
        if(data){
          data.map(pipe => {
            const coords = pipe.points.map(point => {
              return [point.lat, point.lng]
            })
            let lineColor = '#000000';
            switch(pipe.type){
              case 'water':
                  lineColor = '#0B9CC9';
                  break;
              case 'heat':
                  lineColor = '#BC2A1F';
                  break;
              case 'sewer':
                  lineColor = '#5DB100';
                  break;
              case 'gas':
                  lineColor = '#C09B1C';
                  break;
            }
            let myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "LineString",
                    coordinates: coords
                },
                // Свойства.
                properties: {
                    // Контент метки.
                    iconContent: pipe.title,
                    hintContent: pipe.title
                }
            }, {
              // Задаем опции геообъекта.
              // Включаем возможность перетаскивания ломаной.
              draggable: false,
              // Цвет линии.
              strokeColor: lineColor,
              // Ширина линии.
              strokeWidth: 5
            });
            this.map.geoObjects.add(myGeoObject)
          })
        }
      })
      this.pointService.items$.subscribe(data => {
        if(data){
          data.map(point => {            
            let myGeoObject = new ymaps.GeoObject({
              // Описание геометрии.
              geometry: {
                  type: "Point",
                  coordinates: [point.lat, point.lng]
              },
              // Свойства.
              properties: {
                  // Контент метки.
                  iconContent: point.title,
                  hintContent: point['type']['title']
              }
          }, {
            preset: 'islands#governmentCircleIcon',
            iconColor: '#3b5998'
          })
          this.map.geoObjects.add(myGeoObject)
          })
        }
      })
      this.service.load();
      this.pointService.load();
    });
  }

  Notify(event){
    if(event){
      this.notification = event;
    }
  }

  AddTrip(){
    this.openModal('add', {
      success: 'Новая труба добавлена',
      danger: 'Что-то пошло не так'
    })
  }

  openModal(action: 'add'|'edit', messages:{success:string, danger:string}, data?:Pipe){
    this.dialog.open(ModifyModalComponent, {
      width: '50%',
      height: '60%',
      position: {top: '2.5%'},
      data: {
        action: action,
        data: data,
        coords: this.coords
      }
    }).afterClosed().subscribe(result => {
      if(result !== undefined){
        if(result){
          this.notification = {
            notice: messages.success,
            type: 'success',
            show: true
          }
        }else{
          this.notification = {
            notice: messages.danger,
            type: 'danger',
            show: true
          }
        }
      }
    });
    return false;
  } 

}
