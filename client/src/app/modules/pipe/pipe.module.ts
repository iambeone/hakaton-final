import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralPipeLayoutComponent } from './layouts/general-pipe-layout/general-pipe-layout.component';
import { PipePageComponent } from './pages/pipe-page/pipe-page.component';
import { PipeRouting } from './pipe.routing';
import { SharedModule } from '@app/shared/shared.module';
import { MaterialModule } from '@app/material.module';
import { ModifyModalComponent } from './modals/modify-modal/modify-modal.component';
import { PipeService } from './services/pipe.service';

@NgModule({
  declarations: [
    ModifyModalComponent,
    GeneralPipeLayoutComponent, 
    PipePageComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    PipeRouting,
    MaterialModule
  ],
  providers: [
    PipeService
  ],
  entryComponents: [
    ModifyModalComponent
  ],
  bootstrap: [ModifyModalComponent]
})
export class PipeModule { }
