import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralPointLayoutComponent } from './layouts/general-point-layout/general-point-layout.component';
import { PointPageComponent } from './pages/point-page/point-page.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralPointLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full', 
        component: PointPageComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointRouting { }