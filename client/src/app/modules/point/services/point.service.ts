import { Injectable } from '@angular/core';
import { BaseApiService } from '@app/shared/services/base-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Point } from "../models/point.model";

@Injectable({
  providedIn: 'root'
})
export class PointService {
  private _items = new BehaviorSubject<Point[]>([]);

  constructor(private api:BaseApiService) {
    
  }

  get items$(): BehaviorSubject<Point[]>{
    return this._items;
  }

  load(filter?:[], all?:boolean){
    if(all){
      filter['all'] = true;
    }
    this.api.get('point', filter).subscribe((data:Point[]) => {
        this._items.next(data);
    })
  }

  add(item:Point){
    return this.api.post('point', item).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }

  delete(id:number){
    return this.api.delete('point/'+id).
      pipe(map(data => {
        this._items.next(this._items.value.filter(item => item.id != id))
      }))
  }

  edit(id:number, payload:Point){
    return this.api.put('point/' + id, payload).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }
}
