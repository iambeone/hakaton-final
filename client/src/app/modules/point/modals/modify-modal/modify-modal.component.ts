import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Point } from '../../models/point.model';
import { PointService } from '../../services/point.service';
import { PointTypeService } from '@app/modules/pointtype/services/pointtype.service';
import { PointType } from '@app/modules/pointtype/models/pointtype.model';
import { OwnerService } from '@app/modules/owner/services/owner.service';
import { Owner } from '@app/modules/owner/models/owner.model';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-modify-modal',
  templateUrl: './modify-modal.component.html',
  styleUrls: ['./modify-modal.component.scss']
})
export class ModifyModalComponent implements OnInit {
  isnew:boolean = true;
  id:number;
  point: string;
  errors:string[] =[];
  types: PointType[] = [];
  owners: Owner[] = [];
  fields:any[] = [];

  modifyForm = new FormGroup({
    title: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    owner: new FormControl('', Validators.required),
    coords: new FormControl(''),    
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {action: 'edit'|'add', data?: Point, point?:string, coords:string}, private dialogRef: MatDialogRef<Inject>, private pointService: PointService, private pointTypeService: PointTypeService, private ownerService:OwnerService) { }

  ngOnInit() {
    if(this.data && this.data.action){
      this.isnew = this.data.action == 'add';
      this.point = this.data.point;
      this.modifyForm.controls.coords.setValue(this.data.coords);
      if(this.data.data && this.data.action == 'edit'){
        // assignin values
        this.modifyForm.controls.title.setValue(this.data.data.title);
        this.modifyForm.controls.type.setValue(this.data.data.type);
        this.modifyForm.controls.owner.setValue(this.data.data.owner);
        this.id = this.data.data.id;
      }
      if(!this.types.length){
        this.pointTypeService.pointtypes$.subscribe( (items:PointType[]) => {
          this.types = items;
          this.modifyForm.controls.type.valueChanges.subscribe(value => {
            Object.keys(this.modifyForm.controls).map(key => {
              if(key.indexOf('field') !== -1){
                this.modifyForm.removeControl(key);
              }
            })
            let current = this.types.find(el => el.id == value)
            this.fields = current.fields;
            this.fields.map((field, index) => {
              this.modifyForm.addControl(field.id, new FormControl())
            })
          })
        })
        this.pointTypeService.load();
      }
      if(!this.owners.length){
        this.ownerService.items$.subscribe( (items:Owner[]) => {
          this.owners = items;
        })
        this.ownerService.load();
      }

    }
  }

  async onSubmit(){
    // sending values
    if(this.isnew){
      this.pointService.add(this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }else{
      this.pointService.edit(this.id, this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }
  }

  Close(result:any) {
    this.dialogRef.close(result);
  }
}
