import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralPointLayoutComponent } from './layouts/general-point-layout/general-point-layout.component';
import { PointPageComponent } from './pages/point-page/point-page.component';
import { PointRouting } from './point.routing';
import { SharedModule } from '@app/shared/shared.module';
import { MaterialModule } from '@app/material.module';
import { ModifyModalComponent } from './modals/modify-modal/modify-modal.component';
import { PointService } from './services/point.service';

@NgModule({
  declarations: [
    ModifyModalComponent,
    GeneralPointLayoutComponent, 
    PointPageComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    MaterialModule,
    PointRouting
  ],
  providers: [
    PointService
  ],
  entryComponents: [
    ModifyModalComponent
  ],
  bootstrap: [ModifyModalComponent]
})
export class PointModule { }
