import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Point } from "../../models/point.model";
import { PointService } from "../../services/point.service";
import { ModifyModalComponent } from "../../modals/modify-modal/modify-modal.component";
import { MatDialog } from '@angular/material';
import { PipeService } from "@app/modules/pipe/services/pipe.service";
import { Pipe } from "@app/modules/pipe/models/pipe.model";

declare var ymaps:any;

@Component({
  selector: 'app-point-page',
  templateUrl: './point-page.component.html',
  styleUrls: ['./point-page.component.scss']
})
export class PointPageComponent implements OnInit {
  public map :any;
  items: Point[];
  title: string = 'Объекты инфраструктуры';
  coords: string = '';
  disabled:boolean = true;
  notification:{
    notice?: string,
    type?: 'danger' | 'success',
    show:boolean
  } = {show:false}


  constructor(private pipeService:PipeService, private service:PointService, public dialog: MatDialog, private ChangeDetectorRef:ChangeDetectorRef) { }

  ngOnInit() {
    ymaps.ready().then(() => {
      this.map = new ymaps.Map('map', {
        center: [55.797224, 49.108215],
        zoom: 15
      });
      var myCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'islands#governmentCircleIcon',
        iconColor: '#3b5998'
      });
      this.map.events.add('click', e => {
        this.disabled = false;
        var coords = e.get('coords');        
        this.coords = (coords[0].toPrecision(8) + ', ' + coords[1].toPrecision(8))
        this.ChangeDetectorRef.detectChanges()
        myCollection.removeAll()
        myCollection.add(new ymaps.Placemark(coords))
        this.map.geoObjects.add(myCollection)
      })
      this.service.items$.subscribe(data => {
        if(data){
          this.items = data;
          this.items.map(point => {
            let myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "Point",
                    coordinates: [point.lat, point.lng]
                },
                properties: {
                    iconContent: point.title,
                    hintContent: point['type']['title']                
                }
            }, {
                preset: 'islands#blackStretchyIcon',
                draggable: false
            })
            this.map.geoObjects.add(myGeoObject)
          })
        }
      })
      
      this.pipeService.items$.subscribe(data => {
        if(data){
          data.map(pipe => {
            const coords = pipe.points.map(point => {
              return [point.lat, point.lng]
            })
            let lineColor = '#000000';
            switch(pipe.type){
              case 'water':
                  lineColor = '#0B9CC9';
                  break;
              case 'heat':
                  lineColor = '#BC2A1F';
                  break;
              case 'sewer':
                  lineColor = '#5DB100';
                  break;
              case 'gas':
                  lineColor = '#C09B1C';
                  break;
            }
            let myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "LineString",
                    coordinates: coords
                },
                properties: {
                    iconContent: pipe.title,
                    hintContent: pipe.title
                }
            }, {
              draggable: false,
              strokeColor: lineColor,
              strokeWidth: 5
            });
            this.map.geoObjects.add(myGeoObject)
          })
        }
      })
    });
    this.pipeService.load();
    this.service.load();
  }

  Notify(event){
    if(event){
      this.notification = event;
    }
  }

  Edit(event, item:Point){
    event.preventDefault();
    this.openModal('edit', {
      success: 'Объект инфраструктуры обновлён',
      danger: 'Что-то пошло не так'
    }, item)
  }

  AddTrip(){
    this.openModal('add', {
      success: 'Новый объект инфраструктуры добавлен',
      danger: 'Что-то пошло не так'
    })
  }

  openModal(action: 'add'|'edit', messages:{success:string, danger:string}, data?:Point){
    this.dialog.open(ModifyModalComponent, {
      width: '50%',
      height: '60%',
      position: {top: '2.5%'},
      data: {
        action: action,
        data: data,
        coords: this.coords
      }
    }).afterClosed().subscribe(result => {
      if(result !== undefined){
        if(result){
          this.notification = {
            notice: messages.success,
            type: 'success',
            show: true
          }
        }else{
          this.notification = {
            notice: messages.danger,
            type: 'danger',
            show: true
          }
        }
      }
    });
    return false;
  } 

}
