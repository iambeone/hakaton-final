export interface Point {
    id: number
    title: string
    lng: string
    lat: string
    type: number
    owner: number
}
