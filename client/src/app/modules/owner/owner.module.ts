import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralOwnerLayoutComponent } from './layouts/general-owner-layout/general-owner-layout.component';
import { OwnerPageComponent } from './pages/owner-page/owner-page.component';
import { OwnerRouting } from './owner.routing';
import { SharedModule } from '@app/shared/shared.module';
import { ModifyModalComponent } from './modals/modify-modal/modify-modal.component';
import { OwnerService } from './services/owner.service';

@NgModule({
  declarations: [
    ModifyModalComponent,
    GeneralOwnerLayoutComponent, 
    OwnerPageComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    OwnerRouting
  ],
  providers: [
    OwnerService
  ],
  entryComponents: [
    ModifyModalComponent
  ],
  bootstrap: [ModifyModalComponent]
})
export class OwnerModule { }
