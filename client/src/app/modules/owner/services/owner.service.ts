import { Injectable } from '@angular/core';
import { BaseApiService } from '@app/shared/services/base-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Owner } from "../models/owner.model";

@Injectable({
  providedIn: 'root'
})
export class OwnerService {
  private _items = new BehaviorSubject<Owner[]>([]);

  constructor(private api:BaseApiService) {    
  }

  get items$(): BehaviorSubject<Owner[]>{
    return this._items;
  }

  load(filter?:[], all?:boolean){
    if(all){
      filter['all'] = true;
    }
    this.api.get('owner', filter).subscribe((data:Owner[]) => {
        this._items.next(data);
    })
  }


  add(item:Owner){
    return this.api.post('owner', item).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }

  delete(id:number){
    return this.api.delete('owner/'+id).
      pipe(map(data => {
        this._items.next(this._items.value.filter(item => item.id != id))
      }))
  }

  edit(id:number, payload:Owner){
    return this.api.put('owner/' + id, payload).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }
}
