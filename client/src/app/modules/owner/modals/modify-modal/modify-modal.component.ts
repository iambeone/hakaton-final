import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Owner } from '../../models/owner.model';
import { OwnerService } from '../../services/owner.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-modify-modal',
  templateUrl: './modify-modal.component.html',
  styleUrls: ['./modify-modal.component.scss']
})
export class ModifyModalComponent implements OnInit {
  isnew:boolean = true;
  id:number;
  errors:string[] =[];

  modifyForm = new FormGroup({
    title: new FormControl('', Validators.required),
    phone: new FormControl(''),
    email: new FormControl(''),
    legal: new FormControl(''),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {action: 'edit'|'add', data?: Owner}, private dialogRef: MatDialogRef<Inject>, private pointTypeService: OwnerService) { }

  ngOnInit() {
    if(this.data && this.data.action){
      this.isnew = this.data.action == 'add';
      if(this.data.data && this.data.action == 'edit'){
        // assignin values
        this.modifyForm.controls.title.setValue(this.data.data.title);
        this.modifyForm.controls.phone.setValue(this.data.data.phone);
        this.modifyForm.controls.email.setValue(this.data.data.email);
        this.modifyForm.controls.legal.setValue(this.data.data.legal);
        this.id = this.data.data.id;
      }
    }
  }

  async onSubmit(){
    // sending values
    if(this.isnew){
      this.pointTypeService.add(this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }else{
      this.pointTypeService.edit(this.id, this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }
  }

  Close(result:any) {
    this.dialogRef.close(result);
  }
}
