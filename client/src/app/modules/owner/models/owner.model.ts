export interface Owner {
    id: number;
    title: string
    email: string
    phone: string
    legal: string
}
