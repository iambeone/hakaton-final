import { Component, OnInit } from '@angular/core';
import { Owner } from "../../models/owner.model";
import { OwnerService } from "../../services/owner.service";
import { ModifyModalComponent } from "../../modals/modify-modal/modify-modal.component";
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-owner-page',
  templateUrl: './owner-page.component.html',
  styleUrls: ['./owner-page.component.scss']
})
export class OwnerPageComponent implements OnInit {
  items: Owner[];
  title: string = 'Владельцы';
  notification:{
    notice?: string,
    type?: 'danger' | 'success',
    show:boolean
  } = {show:false}  

  constructor(private service:OwnerService, public dialog: MatDialog) { }

  ngOnInit() {
    this.service.items$.subscribe(data => {
      this.items = data;
    })
    this.service.load();
  }

  Notify(event){
    if(event){
      this.notification = event;
    }
  }

  Edit(event, item:Owner){
    event.preventDefault();
    this.openModal('edit', {
      success: 'Владелец отредактирован',
      danger: 'Что-то пошло не так'
    }, item)
  }

  AddTrip(){
    this.openModal('add', {
      success: 'Новый владелец добавлен',
      danger: 'Что-то пошло не так'
    })
  }

  openModal(action: 'add'|'edit', messages:{success:string, danger:string}, data?:Owner){
    this.dialog.open(ModifyModalComponent, {
      width: '50%',
      height: '60%',
      position: {top: '2.5%'},
      data: {
        action: action,
        data: data
      }
    }).afterClosed().subscribe(result => {
      if(result !== undefined){
        if(result){
          this.notification = {
            notice: messages.success,
            type: 'success',
            show: true
          }
        }else{
          this.notification = {
            notice: messages.danger,
            type: 'danger',
            show: true
          }
        }
      }
    });
    return false;
  } 

}
