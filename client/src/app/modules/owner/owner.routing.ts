import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralOwnerLayoutComponent } from './layouts/general-owner-layout/general-owner-layout.component';
import { OwnerPageComponent } from './pages/owner-page/owner-page.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralOwnerLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full', 
        component: OwnerPageComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnerRouting { }