import { Injectable } from '@angular/core';
import { BaseApiService } from '@app/shared/services/base-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sensor } from "../models/sensor.model";

@Injectable({
  providedIn: 'root'
})
export class SensorService {
  private _items = new BehaviorSubject<Sensor[]>([]);

  constructor(private api:BaseApiService) {
  }

  get items$(): BehaviorSubject<Sensor[]>{
    return this._items;
  }

  load(filter?:[], all?:boolean){
    if(all){
      filter['all'] = true;
    }
    this.api.get('sensor/', filter).subscribe((data:Sensor[]) => {
        this._items.next(data);
    })
  }

  add(Sensor:Sensor){
    return this.api.post('sensor', Sensor).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }

  delete(id:number){
    return this.api.delete('sensor/'+id).
      pipe(map(data => {
        this._items.next(this._items.value.filter(sensor => sensor.id != id))
      }))
  }

  edit(id:number, payload:Sensor){
    return this.api.put('sensor/' + id, payload).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._items.next(data);
          return true;
        }
        return false;
      }))
  }
}
