import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Sensor } from "../../models/sensor.model";
import { SensorService } from "../../services/sensor.service";
import { ModifyModalComponent } from "../../modals/modify-modal/modify-modal.component";
import { MatDialog } from '@angular/material';
import { PipeService } from "@app/modules/pipe/services/pipe.service";
import { PointService } from '@app/modules/point/services/point.service';

declare var ymaps:any;

@Component({
  selector: 'app-sensor-page',
  templateUrl: './sensor-page.component.html',
  styleUrls: ['./sensor-page.component.scss']
})
export class SensorPageComponent implements OnInit {
  public map :any;
  items: Sensor[];
  title: string = 'Датчики';
  notification:{
    notice?: string,
    type?: 'danger' | 'success',
    show:boolean
  } = {show:false}  
  coords: string = '';
  disabled:boolean = true;

  constructor(private pipeService:PipeService, private pointService:PointService, private service:SensorService, public dialog: MatDialog, private ChangeDetectorRef:ChangeDetectorRef) { }

  ngOnInit() {
    this.service.items$.subscribe(data => {
      this.items = data;
    })
    this.service.load();
    ymaps.ready().then(() => {
      this.map = new ymaps.Map('map', {
        center: [55.797224, 49.108215],
        zoom: 15
      });
      var myCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'islands#circleIcon',
        iconColor: '#52E20F'
      });
      this.map.events.add('click', e => {
        this.disabled = false;
        var coords = e.get('coords');        
        this.coords = (coords[0].toPrecision(8) + ', ' + coords[1].toPrecision(8))
        this.ChangeDetectorRef.detectChanges()
        myCollection.removeAll()
        myCollection.add(new ymaps.Placemark(coords))
        this.map.geoObjects.add(myCollection)
      })
      
      this.pipeService.items$.subscribe(data => {
        if(data){
          data.map(pipe => {
            const coords = pipe.points.map(point => {
              return [point.lat, point.lng]
            })
            let lineColor = '#000000';
            switch(pipe.type){
              case 'water':
                  lineColor = '#0B9CC9';
                  break;
              case 'heat':
                  lineColor = '#BC2A1F';
                  break;
              case 'sewer':
                  lineColor = '#5DB100';
                  break;
              case 'gas':
                  lineColor = '#C09B1C';
                  break;
            }
            let myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "LineString",
                    coordinates: coords
                },
                // Свойства.
                properties: {
                    // Контент метки.
                    iconContent: pipe.title,
                    hintContent: pipe.title
                }
            }, {
              // Задаем опции геообъекта.
              // Включаем возможность перетаскивания ломаной.
              draggable: false,
              // Цвет линии.
              strokeColor: lineColor,
              // Ширина линии.
              strokeWidth: 5
            });
            this.map.geoObjects.add(myGeoObject)
          })
        }
      })
      this.pointService.items$.subscribe(data => {
        if(data){
          data.map(point => {            
            let myGeoObject = new ymaps.GeoObject({
              // Описание геометрии.
              geometry: {
                  type: "Point",
                  coordinates: [point.lat, point.lng]
              },
              // Свойства.
              properties: {
                  // Контент метки.
                  iconContent: point.title,
                  hintContent: point['type']['title']
              }
          }, {
            preset: 'islands#governmentCircleIcon',
            iconColor: '#3b5998'
          })
          this.map.geoObjects.add(myGeoObject)
          })
        }
      })
      var sensorCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'islands#circleIcon'
      });
      this.service.items$.subscribe(data => {
        if(data){
          sensorCollection.removeAll();
          data.map(point => {
            let color = '#52E20F'
            if(point.value >= point.warn){
              color = '#E2DC0F'
            }
            if(point.value >= point.risk){
              color = '#E2340F'
            }
            let myGeoObject = new ymaps.GeoObject({
              // Описание геометрии.
              geometry: {
                  type: "Point",
                  coordinates: [point.lat, point.lng]
              },
              // Свойства.
              properties: {
                  // Контент метки.
                  hintContent: point['type']['title']
              }
              }, {
                preset: 'islands#CircleDotIcon',
                iconColor: color
              })
          sensorCollection.add(myGeoObject)
          this.map.geoObjects.add(sensorCollection)
          })
        }
      })
      this.pipeService.load();
      this.pointService.load();
      this.service.load()
    });
  }

  Notify(event){
    if(event){
      this.notification = event;
    }
  }

  Add(){
    this.openModal('add', {
      success: 'Новый тип датчик',
      danger: 'Что-то пошло не так'
    })
  }

  openModal(action: 'add'|'edit', messages:{success:string, danger:string}, data?:Sensor){
    this.dialog.open(ModifyModalComponent, {
      width: '50%',
      height: '60%',
      position: {top: '2.5%'},
      data: {
        action: action,
        data: data,
        coords: this.coords
      }
    }).afterClosed().subscribe(result => {
      if(result !== undefined){
        if(result){
          this.notification = {
            notice: messages.success,
            type: 'success',
            show: true
          }
        }else{
          this.notification = {
            notice: messages.danger,
            type: 'danger',
            show: true
          }
        }
      }
    });
    return false;
  }

  

}
