export interface Sensor {
    id: number;    
    title: string
    type?: string
    max?: number
    min?: number
    value?: number
    risk?: number
    warn?: number
    owner?: number
    lat?: number
    lng?: number
}
