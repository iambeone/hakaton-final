import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralSensorLayoutComponent } from './layouts/general-sensor-layout/general-sensor-layout.component';
import { SensorPageComponent } from './pages/sensor-page/sensor-page.component';
import { SensorRouting } from './sensor.routing';
import { SharedModule } from '@app/shared/shared.module';
import { ModifyModalComponent } from './modals/modify-modal/modify-modal.component';
import { SensorService } from './services/sensor.service';
import { MaterialModule } from '@app/material.module';

@NgModule({
  declarations: [
    ModifyModalComponent,
    GeneralSensorLayoutComponent, 
    SensorPageComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    SensorRouting,
    MaterialModule
  ],
  providers: [
    SensorService
  ],
  entryComponents: [
    ModifyModalComponent
  ],
  bootstrap: [ModifyModalComponent]
})
export class SensorModule { }
