import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralSensorLayoutComponent } from './layouts/general-sensor-layout/general-sensor-layout.component';
import { SensorPageComponent } from './pages/sensor-page/sensor-page.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralSensorLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full', 
        component: SensorPageComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SensorRouting { }