import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Sensor } from '../../models/sensor.model';
import { SensorService } from '../../services/sensor.service';
import { OwnerService } from '@app/modules/owner/services/owner.service';
import { Owner } from '@app/modules/owner/models/owner.model';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-modify-modal',
  templateUrl: './modify-modal.component.html',
  styleUrls: ['./modify-modal.component.scss']
})
export class ModifyModalComponent implements OnInit {
  isnew:boolean = true;
  id:number;
  errors:string[] =[];
  owners: Owner[] = [];

  modifyForm = new FormGroup({
    title: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    owner: new FormControl(''),
    coords: new FormControl(''),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {action: 'edit'|'add', data?: Sensor, coords:string}, private dialogRef: MatDialogRef<Inject>, private sensorService: SensorService, private ownerService:OwnerService) { }

  ngOnInit() {
    if(this.data && this.data.action){
      this.isnew = this.data.action == 'add';
      console.log(this.data.coords) 
      this.modifyForm.controls.coords.setValue(this.data.coords);
      if(this.data.data && this.data.action == 'edit'){
        // assignin values
        this.modifyForm.controls.title.setValue(this.data.data.title);
        this.modifyForm.controls.type.setValue(this.data.data.type);
        this.modifyForm.controls.owner.setValue(this.data.data.owner);
        this.id = this.data.data.id;
      }
      if(!this.owners.length){
        this.ownerService.items$.subscribe( (items:Owner[]) => {
          this.owners = items;
        })
        this.ownerService.load();
      }
    }
  }

  async onSubmit(){
    // sending values
    if(this.isnew){
      this.sensorService.add(this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }else{
      this.sensorService.edit(this.id, this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }
  }

  Close(result:any) {
    this.dialogRef.close(result);
  }
}
