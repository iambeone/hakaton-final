import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralPointTypeLayoutComponent } from './layouts/general-pointtype-layout/general-pointtype-layout.component';
import { PointTypePageComponent } from './pages/pointtype-page/pointtype-page.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralPointTypeLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full', 
        component: PointTypePageComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointTypeRouting { }