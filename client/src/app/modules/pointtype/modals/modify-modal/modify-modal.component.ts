import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PointType } from '../../models/pointtype.model';
import { PointTypeService } from '../../services/pointtype.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-modify-modal',
  templateUrl: './modify-modal.component.html',
  styleUrls: ['./modify-modal.component.scss']
})
export class ModifyModalComponent implements OnInit {
  isnew:boolean = true;
  id:number;
  errors:string[] =[];

  modifyForm = new FormGroup({
    title: new FormControl('', Validators.required),
    code: new FormControl('', Validators.required),
    comment: new FormControl(''),
    fields: new FormControl(''),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {action: 'edit'|'add', data?: PointType}, private dialogRef: MatDialogRef<Inject>, private pointTypeService: PointTypeService) { }

  ngOnInit() {
    if(this.data && this.data.action){
      this.isnew = this.data.action == 'add';
      if(this.data.data && this.data.action == 'edit'){
        // assignin values
        this.modifyForm.controls.title.setValue(this.data.data.title);
        this.modifyForm.controls.code.setValue(this.data.data.code);
        this.modifyForm.controls.comment.setValue(this.data.data.comment);
        this.modifyForm.controls.fields.setValue(this.data.data.fields);
        this.id = this.data.data.id;
      }
    }
  }

  async onSubmit(){
    // sending values
    if(this.isnew){
      this.pointTypeService.add(this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }else{
      this.pointTypeService.edit(this.id, this.modifyForm.value).subscribe(result => {
        this.Close(result);
      })
    }
  }

  Close(result:any) {
    this.dialogRef.close(result);
  }
}
