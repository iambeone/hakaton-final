export interface PointType {
    id: number;    
    title: string
    code?: string
    comment?: string
    fields?: any[]
}
