import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralPointTypeLayoutComponent } from './layouts/general-pointtype-layout/general-pointtype-layout.component';
import { PointTypePageComponent } from './pages/pointtype-page/pointtype-page.component';
import { PointTypeRouting } from './pointtype.routing';
import { SharedModule } from '@app/shared/shared.module';
import { ModifyModalComponent } from './modals/modify-modal/modify-modal.component';
import { PointTypeService } from './services/pointtype.service';

@NgModule({
  declarations: [
    ModifyModalComponent,
    GeneralPointTypeLayoutComponent, 
    PointTypePageComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    PointTypeRouting
  ],
  providers: [
    PointTypeService
  ],
  entryComponents: [
    ModifyModalComponent
  ],
  bootstrap: [ModifyModalComponent]
})
export class PointTypeModule { }
