import { Injectable } from '@angular/core';
import { BaseApiService } from '@app/shared/services/base-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PointType } from "../models/pointtype.model";

@Injectable({
  providedIn: 'root'
})
export class PointTypeService {
  private _pointtypes = new BehaviorSubject<PointType[]>([]);

  constructor(private api:BaseApiService) {
  }

  get pointtypes$(): BehaviorSubject<PointType[]>{
    return this._pointtypes;
  }

  load(filter?:[], all?:boolean){
    if(all){
      filter['all'] = true;
    }
    this.api.get('pointtype/', filter).subscribe((data:PointType[]) => {
        this._pointtypes.next(data);
    })
  }

  add(pointtype:PointType){
    return this.api.post('pointtype', pointtype).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._pointtypes.next(data);
          return true;
        }
        return false;
      }))
  }

  delete(id:number){
    return this.api.delete('pointtype/'+id).
      pipe(map(data => {
        this._pointtypes.next(this._pointtypes.value.filter(pointtype => pointtype.id != id))
      }))
  }

  edit(id:number, payload:PointType){
    return this.api.put('pointtype/' + id, payload).
      pipe(map(data => {
        if(Array.isArray(data)){
          this._pointtypes.next(data);
          return true;
        }
        return false;
      }))
  }
}
