import { Component, OnInit } from '@angular/core';
import { PointType } from "../../models/pointtype.model";
import { PointTypeService } from "../../services/pointtype.service";
import { FormGroup, FormControl } from '@angular/forms';
import { ModifyModalComponent } from "../../modals/modify-modal/modify-modal.component";
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-pointtype-page',
  templateUrl: './pointtype-page.component.html',
  styleUrls: ['./pointtype-page.component.scss']
})
export class PointTypePageComponent implements OnInit {
  pointtypes: PointType[];
  title: string = 'Типы оборудования';
  notification:{
    notice?: string,
    type?: 'danger' | 'success',
    show:boolean
  } = {show:false}  

  constructor(private service:PointTypeService, public dialog: MatDialog) { }

  ngOnInit() {
    this.service.pointtypes$.subscribe(data => {
      this.pointtypes = data;
    })
    this.service.load();
  }

  Notify(event){
    if(event){
      this.notification = event;
    }
  }

  AddTrip(){
    this.openModal('add', {
      success: 'Новый тип оборудования добавлен',
      danger: 'Что-то пошло не так'
    })
  }

  openModal(action: 'add'|'edit', messages:{success:string, danger:string}, data?:PointType){
    this.dialog.open(ModifyModalComponent, {
      width: '50%',
      height: '60%',
      position: {top: '2.5%'},
      data: {
        action: action,
        data: data
      }
    }).afterClosed().subscribe(result => {
      if(result !== undefined){
        if(result){
          this.notification = {
            notice: messages.success,
            type: 'success',
            show: true
          }
        }else{
          this.notification = {
            notice: messages.danger,
            type: 'danger',
            show: true
          }
        }
      }
    });
    return false;
  }

  

}
