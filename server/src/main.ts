import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config } from './_config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();  
  await app.listen(config.app.port ? config.app.port : 4200);
}
bootstrap();
