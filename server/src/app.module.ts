import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { TaskModule } from './modules/task/task.module';
import { OwnerModule } from './modules/owner/owner.module';
import { PipeModule } from './modules/pipe/pipe.module';
import { PointModule } from './modules/point/point.module';
import { PointTypeModule } from './modules/pointtype/pointtype.module';
import { SensorModule } from './modules/sensor/sensor.module';

@Module({
  imports: [UserModule, TaskModule, OwnerModule, PipeModule, PointTypeModule, PointModule, SensorModule ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
