import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { Point } from '../../DB/entity/point.entity';
import { PointDTO } from '../../dto/point.dto';

@Injectable()
export class PointService {
  constructor(
  ) { }

    // get the results
    async list(payload?:PointDTO.Payload){
      let qb = getRepository(Point)
      .createQueryBuilder("point")
      qb.orderBy('point.id')
      .innerJoinAndSelect('point.type', 'Type')
      .innerJoinAndSelect('point.owner', 'Onwer')
      return (await qb.getMany()).map(point => {
        point.lat = parseFloat(point.lat.toString())
        point.lng = parseFloat(point.lng.toString())
        return point;
      })
    }

    // delete record
    async delete(id:number){
      let res = await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Point)
      .where("id = :id", {id : id})
      .execute();
    }

    // create record
    async create(payload:PointDTO.Payload){
      let [lat, lng] = payload.coords.split(',').map(t => t.trim());
      return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Point)
      .values({
        lat: parseFloat(lat),
        lng: parseFloat(lng),
        title: payload.title,
        owner: {id : payload.owner},
        type: {id : payload.type},
      })
      .execute()
      .then(() => {
          return this.list();
      })
    }

    // update record
    async update(payload:PointDTO.Payload, id:number){
      return getRepository(Point)
        .createQueryBuilder("point")
        .update()
        .set({
          title: payload.title
        })
        .where("id = :id", {id : id})
        .execute()
        .then(() => {
          return this.list();
        })
    }
}