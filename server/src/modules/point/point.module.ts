import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { PointProviders } from './point.provider';
import { PointService } from './point.service';
import { PointController } from './point.controller';

@Module({
  imports: [dbModule],
  controllers: [PointController],
  providers: [...PointProviders, PointService, PointController],
  exports: [...PointProviders, PointService, PointController]
})
export class PointModule { } 