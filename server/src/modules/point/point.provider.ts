import { Connection } from 'typeorm';
import { Point } from '../../DB/entity/point.entity';

export const PointProviders = [
  {
    provide: 'PointRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(Point),
    inject: ['DbConnectionToken'],
  },
];
