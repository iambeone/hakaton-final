import { Param, Controller, Post, Body, Delete, Get, Query, Put } from '@nestjs/common';
import { PointService } from './point.service';

@Controller('point')
export class PointController {
  constructor(private readonly service: PointService) { }
  
  @Get('')
  async list(@Query() payload) {
    return await this.service.list(payload);
  }
  
  @Delete(':id')
  async delete(@Param('id') id) {
    return await this.service.delete(id);
  }
    
  @Put(':id')
  async update(@Body() payload, @Param('id') id) {
    return await this.service.update(payload, id);
  }
  
  @Post('')
  async create(@Body() payload) {
    return await this.service.create(payload);
  }

} 