import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { PointType } from '../../DB/entity/pointtype.entity';
import { PointTypeDTO } from '../../dto/pointtype.dto';

@Injectable()
export class PointTypeService {
  constructor(
  ) { }

    // get the results
    async list(payload?:PointTypeDTO.Payload){
      let qb = getRepository(PointType)
      .createQueryBuilder("point") 
      qb.orderBy('id')    
      return await qb.getMany()
    }

    // delete record
    async delete(id:number){
      let res = await getConnection()
      .createQueryBuilder()
      .delete()
      .from(PointType)
      .where("id = :id", {id : id})
      .execute();
    }

    // create record
    async create(payload:PointTypeDTO.Payload){
      let i = 0;
      const fields = payload.fields ? payload.fields.split("\n").map(str => {
        let [_name, _values] = str.split(':');
        let values = []
        let type = '';
        if(_values){
          values = _values.trim().split(',').map(v => v.trim())
        }
        switch(_name[0]){
          case '!':
            type = 'checkbox'
            break;
          case '@':
            type = 'dropdown'
            break;
          case '#':
            type = 'options'
            break;
          case '$':
            type = 'area'
            break;
          default:
            type = 'text'
            break;
        }
        return {
          name : type !== 'text' ? _name.substr(1) : _name,
          type : type,
          values : values,
          id: `field${++i}`
        }
      }) : []
      return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(PointType)
      .values({
        title: payload.title,
        code: payload.code,
        comment: payload.comment,
        fields: fields
      })
      .execute()
      .then(() => {
          return this.list();
      })
    }

    // update record
    async update(payload:PointTypeDTO.Payload, id:number){
      return getRepository(PointType)
        .createQueryBuilder("point")
        .update()
        .set({
          title: payload.title
        })
        .where("id = :id", {id : id})
        .execute()
        .then(() => {
          return this.list();
        })
    }
}