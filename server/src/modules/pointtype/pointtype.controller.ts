import { Param, Controller, Post, Body, Delete, Get, Query, Put } from '@nestjs/common';
import { PointTypeService } from './pointtype.service';

@Controller('pointtype')
export class PointTypeController {
  constructor(private readonly service: PointTypeService) { }
  
  @Get('')
  async list(@Query() payload) {
    return await this.service.list(payload);
  }
  
  @Delete(':id')
  async delete(@Param('id') id) {
    return await this.service.delete(id);
  }
    
  @Put(':id')
  async update(@Body() payload, @Param('id') id) {
    return await this.service.update(payload, id);
  }
  
  @Post('')
  async create(@Body() payload) {
    return await this.service.create(payload);
  }

} 