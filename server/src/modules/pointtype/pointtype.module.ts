import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { PointTypeProviders } from './pointtype.provider';
import { PointTypeService } from './pointtype.service';
import { PointTypeController } from './pointtype.controller';

@Module({
  imports: [dbModule],
  controllers: [PointTypeController],
  providers: [...PointTypeProviders, PointTypeService, PointTypeController],
  exports: [...PointTypeProviders, PointTypeService, PointTypeController]
})
export class PointTypeModule { } 