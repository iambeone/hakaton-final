import { Connection } from 'typeorm';
import { PointType } from '../../DB/entity/pointtype.entity';

export const PointTypeProviders = [
  {
    provide: 'PointTypeRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(PointType),
    inject: ['DbConnectionToken'],
  },
];
