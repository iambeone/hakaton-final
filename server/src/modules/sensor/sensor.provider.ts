import { Connection } from 'typeorm';
import { Sensor } from '../../DB/entity/sensor.entity';

export const SensorProviders = [
  {
    provide: 'SensorRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(Sensor),
    inject: ['DbConnectionToken'],
  },
];
