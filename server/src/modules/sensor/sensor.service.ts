import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { Sensor } from '../../DB/entity/sensor.entity';
import { SensorDTO } from '../../dto/sensor.dto';

@Injectable()
export class SensorService {
  constructor(
  ) { }

    // get the results
    async list(payload?:SensorDTO.Payload){
      let qb = getRepository(Sensor)
      .createQueryBuilder("Sensor") 
      qb.orderBy('id')    
      return (await qb.getMany()).map(sensor => {
        sensor.value = (sensor.min + Math.random() * (sensor.max + 1 - sensor.min));
        return sensor;
      })
    }

    // delete record
    async delete(id:number){
      let res = await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Sensor)
      .where("id = :id", {id : id})
      .execute();
    }

    // create record
    async create(payload:SensorDTO.Payload){
      let [lat, lng] = payload.coords.split(',').map(t => t.trim());
      return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Sensor)
      .values({
        lat: lat,
        lng: lng,
        title: payload.title,
        owner: {id : payload.owner},
        type: payload.type
      })
      .execute()
      .then(() => {
          return this.list();
      })
    }

    // update record
    async update(payload:SensorDTO.Payload, id:number){
      return getRepository(Sensor)
        .createQueryBuilder("Sensor")
        .update()
        .set({
          title: payload.title
        })
        .where("id = :id", {id : id})
        .execute()
        .then(() => {
          return this.list();
        })
    }
}