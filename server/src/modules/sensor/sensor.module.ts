import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { SensorProviders } from './sensor.provider';
import { SensorService } from './sensor.service';
import { SensorController } from './sensor.controller';

@Module({
  imports: [dbModule],
  controllers: [SensorController],
  providers: [...SensorProviders, SensorService, SensorController],
  exports: [...SensorProviders, SensorService, SensorController]
})
export class SensorModule { } 