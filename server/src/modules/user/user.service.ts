import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { User } from '../../DB/entity/user.entity';
import { UserDTO } from '../../dto/user.dto';
const bcrypt = require('bcrypt');

@Injectable()
export class UserService {
  constructor(
  ) { }

    async updatePwd(pwd:string, user:number){
        const saltRounds = 10;
        return bcrypt.genSalt(saltRounds, function(err, salt) {
            return bcrypt.hash(pwd, salt, function(err, hash) {
                return getRepository(User)
                .createQueryBuilder("user")
                .update()
                .set({hash: hash})
                .where("id = :id", {id : user})
                .execute();
            });
        });
    }

    async comparePwdWithHash(pwd:string, user:User){
        return bcrypt.compare(pwd, user.hash);
    }

    async findOne(email:string){
        return await getRepository(User)
        .createQueryBuilder("user")
        .where('email = :email', {email:email})
        .getOne();
    }

    async register(user:UserDTO.RequestPayload){
        let checkUser = await this.findOne(user.email);        
        if(checkUser){
            return false;
        }
        return await getConnection()
        .createQueryBuilder()
        .insert()
        .into(User)
        .values({ email: user.email, role: 0 })
        .returning('id')
        .execute()        
        .then(data => {
            if(data.identifiers.length){
                return this.updatePwd(user.password, data.identifiers[0].id).then(() => true)
            }else{
                return false;
            }            
        })
    }

    // update record
    async update(user:UserDTO.RequestPayload, payload:UserDTO.RequestPayload, id:number){
        const userData = {};
        Object.keys(payload).map(key => {
            if(typeof payload[key] != 'undefined' && key != 'password'){
                userData[key] = payload[key]
            }
        })
      return getRepository(User)
        .createQueryBuilder("user")
        .update()
        .set(userData)
        .where("id = :id", {id : id})
        .returning('*')
        .execute()        
        .then(data => {
            if(payload.password && payload.password.length){
                return this.updatePwd(payload.password, id).then(() => data['raw'][0])
            }else{
                return data['raw'][0];
            }
          
        })
    }

    async list(user:UserDTO.RequestPayload, payload?:UserDTO.Payload){
        let qb = getRepository(User)
        .createQueryBuilder("user")
        .where('id != :user_id', {user_id : user.id})
        if(payload){
          let keys = Object.keys(payload).filter(key => payload[key] !== '' && payload[key] !== 'null' && payload[key] !== 'false').map(key => {              
              if(key == 'showAdmin'){
                  return 2;
              }
              if(key == 'showModer'){
                return 1;
              }
              if(key == 'showUser'){
                return 0;
              }
        });
        if(keys.length){
            qb.andWhere('role IN (:...ids)', {ids:keys})
        }
        }
        qb.orderBy('id')
        return await qb.getMany()
        
    }
}