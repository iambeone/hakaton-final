import { Get, Controller, Post, Body, Param, Put, Query, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from "../../decorators/user.decorator";
import { roles } from '../../decorators/roles.decorator';

@Controller('user')
export class UserController {
  constructor(
    private readonly service: UserService,
  ) { }
  
  @Post('register')
  async create(@Body() payload) {
    return await this.service.register(payload);
  }
  
  @roles('moderator', 'admin')
  @Get('')
  async get(@User() user, @Query() payload) {
    return await this.service.list(user, payload);;
  }
  
  @roles('moderator', 'admin')
  @Put(':id')
  async update(@User() user, @Body() payload, @Param('id') id) {
    return await this.service.update(user, payload, id);
  }
} 