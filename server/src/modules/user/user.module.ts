import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { UserProviders } from './user.provider';
import { UserService } from './user.service';
import { UserController } from './user.controller';

@Module({
  imports: [dbModule],
  controllers: [UserController],
  providers: [...UserProviders, UserService, UserController],
  exports: [...UserProviders, UserService, UserController]
})
export class UserModule { } 