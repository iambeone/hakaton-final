import { Connection } from 'typeorm';
import { User } from '../../DB/entity/user.entity';

export const UserProviders = [
  {
    provide: 'UserRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(User),
    inject: ['DbConnectionToken'],
  },
];
