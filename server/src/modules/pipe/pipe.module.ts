import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { PipeProviders } from './pipe.provider';
import { PipeService } from './pipe.service';
import { PipeController } from './pipe.controller';

@Module({
  imports: [dbModule],
  controllers: [PipeController],
  providers: [...PipeProviders, PipeService, PipeController],
  exports: [...PipeProviders, PipeService, PipeController]
})
export class PipeModule { } 