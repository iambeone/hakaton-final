import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { Pipe } from '../../DB/entity/pipe.entity';
import { PipeDTO } from '../../dto/pipe.dto';

@Injectable()
export class PipeService {
  constructor(
  ) { }

    // get the results
    async list(payload?:PipeDTO.Payload){
      let qb = getRepository(Pipe)
      .createQueryBuilder("pipe") 
      qb.orderBy('pipe.id')
      .innerJoinAndSelect('pipe.owner', 'Owner')
      return (await qb.getMany()).map((pipe:Pipe) => {
        pipe.points = pipe.points.map(point => {          
          return {
            lat: point[0],
            lng: point[1],
          }})
        return pipe;
      })
    }

    // delete record
    async delete(id:number){
      let res = await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Pipe)
      .where("id = :id", {id : id})
      .execute();
    }

    // create record
    async create(payload:PipeDTO.Payload){
      return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Pipe)
      .values({ 
        title: payload.title,
        owner: { id: payload.owner },
        points: payload.coords.map(pair => pair.map(i => parseFloat(i))),
        type: payload.type,
        comment: payload.comment,
        constType: payload.constType,
        constYear: payload.constYear,
        depreciation: payload.depreciation
      })
      .execute()
      .then(() => {
          return this.list();
      })
    }

    // update record
    async update(payload:PipeDTO.Payload, id:number){
      return getRepository(Pipe)
        .createQueryBuilder("owner")
        .update()
        .set({
          title: payload.title
        })
        .where("id = :id", {id : id})
        .execute()
        .then(() => {
          return this.list();
        })
    }
}