import { Connection } from 'typeorm';
import { Pipe } from '../../DB/entity/pipe.entity';

export const PipeProviders = [
  {
    provide: 'PipeRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(Pipe),
    inject: ['DbConnectionToken'],
  },
];
