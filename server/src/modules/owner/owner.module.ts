import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { OwnerProviders } from './owner.provider';
import { OwnerService } from './owner.service';
import { OwnerController } from './owner.controller';

@Module({
  imports: [dbModule],
  controllers: [OwnerController],
  providers: [...OwnerProviders, OwnerService, OwnerController],
  exports: [...OwnerProviders, OwnerService, OwnerController]
})
export class OwnerModule { } 