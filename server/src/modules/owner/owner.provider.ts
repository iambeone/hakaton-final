import { Connection } from 'typeorm';
import { Owner } from '../../DB/entity/owner.entity';

export const OwnerProviders = [
  {
    provide: 'OwnerRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(Owner),
    inject: ['DbConnectionToken'],
  },
];
