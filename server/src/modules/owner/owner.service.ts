import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { Owner } from '../../DB/entity/owner.entity';
import { OwnerDTO } from '../../dto/owner.dto';

@Injectable()
export class OwnerService {
  constructor(
  ) { }

    // get the results
    async list(payload?:OwnerDTO.Payload){
      let qb = getRepository(Owner)
      .createQueryBuilder("pipowner") 
      qb.orderBy('id')    
      return await qb.getMany()
    }

    // delete record
    async delete(id:number){
      let res = await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Owner)
      .where("id = :id", {id : id})
      .execute();
    }

    // create record
    async create(payload:OwnerDTO.Payload){
      return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Owner)
      .values({
        title: payload.title,
        email: payload.email,
        phone: payload.phone,
        legal: payload.legal
      })
      .execute()
      .then(() => {
          return this.list();
      })
    }

    // update record
    async update(payload:OwnerDTO.Payload, id:number){
      return getRepository(Owner)
        .createQueryBuilder("owner")
        .update()
        .set({
          title: payload.title,
          email: payload.email,
          phone: payload.phone,
          legal: payload.legal
        })
        .where("id = :id", {id : id})
        .execute()
        .then(() => {
          return this.list();
        })
    }
}