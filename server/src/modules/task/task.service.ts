import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { Task } from '../../DB/entity/task.entity';
import { TaskDTO } from '../../dto/task.dto';

@Injectable()
export class TaskService {
  constructor(
  ) { }

    // get the results
    async list(payload?:TaskDTO.Payload){
      let qb = getRepository(Task)
      .createQueryBuilder("trip") 
      if(payload){
        let keys = Object.keys(payload).filter(key => {
          if(key == 'showFuture' || key == 'showPast'){
            return payload[key] !== '' && payload[key] !== 'null' && payload[key] !== 'false'
          }else{
            let date = new Date(payload[key]);
            return Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date.getTime())
          }
        });
        keys.map(key => {
          if(key == 'showFuture' && keys.indexOf('showPast') == -1){
            qb.andWhere('trip."startDate" > NOW()')
          }
          if(key == 'showPast' && keys.indexOf('showFuture') == -1){
            qb.andWhere('trip."startDate" < NOW()')
          }
          if(key == 'startFrom'){
            qb.andWhere('trip."startDate" >= :startFrom', {startFrom: payload.startFrom})
          }
          if(key == 'startTill'){
            qb.andWhere('trip."startDate" <= :startTill', {startTill: payload.startTill})
          }
          if(key == 'endFrom'){
            qb.andWhere('trip."endDate" >= :endFrom', {endFrom: payload.endFrom})
          }
          if(key == 'endTill'){
            qb.andWhere('trip."endDate" <= :endTill', {endTill: payload.endTill})
          }
        })
      }
      qb.orderBy('id')    
      return await qb.getMany()
    }

    async monthPlan(payload?:TaskDTO.MonthPayload){
      let today = new Date();
      payload.month = payload.month ? payload.month - 1 : payload.month;
      let date = new Date(payload.year ? payload.year : today.getFullYear(), payload.month ? payload.month : (today.getMonth()), 1);
      let lastdate = new Date(payload.year ? payload.year : today.getFullYear(), (payload.month ? payload.month : (today.getMonth())) + 1, 0);
      let modifiedPayload:TaskDTO.Payload = {
        startFrom: [date.getFullYear(), ('0' + (date.getMonth() + 1)).slice(-2), date.getDate()].join('-'),
        startTill: [lastdate.getFullYear(), ('0' + (lastdate.getMonth() + 1)).slice(-2), lastdate.getDate()].join('-'),
      }
      return this.list(modifiedPayload);
    }

    // delete record
    async delete(id:number){
      let res = await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Task)
      .where("id = :id", {id : id})
      .execute();
    }

    // create record
    async create(payload:TaskDTO.Object){
      return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Task)
      .values({ 
        title: payload.title,
        comment: payload.comment
      })
      .execute()
      .then(() => {
          return this.list();
      })
    }

    // update record
    async update(payload:TaskDTO.Object, id:number){
      return getRepository(Task)
        .createQueryBuilder("trip")
        .update()
        .set({
          title: payload.title,
          comment: payload.comment
        })
        .where("id = :id", {id : id})
        .execute()
        .then(() => {
          return this.list();
        })
    }
}