import { Connection } from 'typeorm';
import { Task } from '../../DB/entity/task.entity';

export const TaskProviders = [
  {
    provide: 'TaskRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(Task),
    inject: ['DbConnectionToken'],
  },
];
