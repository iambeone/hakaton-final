import { Module } from '@nestjs/common';
import { dbModule } from '../../DB/db.module';
import { TaskProviders } from './task.provider';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';

@Module({
  imports: [dbModule],
  controllers: [TaskController],
  providers: [...TaskProviders, TaskService, TaskController],
  exports: [...TaskProviders, TaskService, TaskController]
})
export class TaskModule { } 