import { Param, Controller, Post, Body, Delete, Get, Query, Put, UseGuards } from '@nestjs/common';
import { TaskService } from './task.service';

@Controller('task')
export class TaskController {
  constructor(private readonly service: TaskService) { }
  
  @Get('')
  async list(@Query() payload) {
    return await this.service.list(payload);
  }
  
  @Get('month')
  async plan(@Query() payload) {
    return await this.service.monthPlan(payload);
  }

  @Delete(':id')
  async delete(@Param('id') id) {
    return await this.service.delete(id);
  }
    
  @Put(':id')
  async update(@Body() payload, @Param('id') id) {
    return await this.service.update(payload, id);
  }
  
  @Post('')
  async create(@Body() payload) {
    return await this.service.create(payload);
  }

} 