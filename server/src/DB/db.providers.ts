import { createConnection } from 'typeorm';
import { config } from '../_config';

export const dbProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async () => await createConnection(config.Database)
  }
]; 