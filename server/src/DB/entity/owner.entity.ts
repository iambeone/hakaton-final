import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToMany } from "typeorm";
import { Pipe } from './pipe.entity';
import { Point } from './point.entity';
import { Sensor } from './sensor.entity';

@Entity()
export class Owner {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "varchar", nullable: true })
    title: string;

    @Column({ type: "varchar", nullable: true })
    email: string;

    @Column({ type: "varchar", nullable: true })
    phone: string;

    @Column({ type: "varchar", nullable: true })
    legal: string;
    
    @OneToMany(type => Pipe, pipe => pipe.owner)
    pipes?: Pipe[];
    
    @OneToMany(type => Point, point => point.owner)
    points?: Point[];

    @OneToMany(type => Sensor, sensor => sensor.owner)
    sensors?: Sensor[];
}