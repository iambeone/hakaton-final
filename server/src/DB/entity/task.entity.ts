import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Task {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "varchar", nullable: true })
    title: string;

    @Column({ type: "varchar", nullable: true })
    address: string;

    @Column({ type: "date", nullable: true })
    date: Date;

    @Column({ type: "text", nullable: true })
    comment?: string;

    @Column({ type: "boolean", nullable: true })
    completed?: boolean;
    
    @ManyToOne(type => User, user => user.tasks)
    @JoinColumn()
    user?: User;
}