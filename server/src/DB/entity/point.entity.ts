import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from "typeorm";
import { PointType } from "./pointtype.entity";
import { Owner } from './owner.entity';

@Entity()
export class Point {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "varchar", nullable: true })
    title: string;

    // координаты
    @Column({ type: "varchar", nullable: true })
    lng: number;

    @Column({ type: "varchar", nullable: true })
    lat: number;

    @ManyToOne(type => PointType)
    @JoinColumn()
    type: PointType;
    
    @ManyToOne(type => Owner, owner => owner.points)
    @JoinColumn()
    owner?: Owner;
}