import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

interface PointTypeField{
    id: string
    name: string
    type: 'text' | 'checkbox' | 'dropdown' | 'area' | 'options' | string
    values?: string[]
}

@Entity()
export class PointType {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "varchar", nullable: true })
    title: string;

    @Column({ type: "varchar", nullable: true })
    code: string;

    @Column({ type: "text", nullable: true })
    comment?: string;

    @Column({ type: "jsonb", nullable: true })
    fields?: PointTypeField[];
}