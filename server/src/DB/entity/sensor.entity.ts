import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from "typeorm";
import { Owner } from './owner.entity';

@Entity()
export class Sensor {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "varchar", nullable: true })
    title: string;

    // координаты
    @Column({ type: "varchar", nullable: true })
    lng: string;

    @Column({ type: "varchar", nullable: true })
    lat: string;

    @Column({
        type: "enum",
        enum: [
            "temperature",
            "pressure",
        ], nullable: true
    })
    type?: "temperature" | "pressure" | string;
    
    @ManyToOne(type => Owner, owner => owner.sensors)
    @JoinColumn()
    owner?: Owner;

    @Column({ type: "int", nullable: true })
    max: number;

    @Column({ type: "int", nullable: true })
    min: number;

    @Column({ type: "int", nullable: true })
    value: number;

    @Column({ type: "int", nullable: true })
    risk: number;

    @Column({ type: "int", nullable: true })
    warn: number;
}