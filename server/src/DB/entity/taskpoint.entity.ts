import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from "typeorm";
import { Task } from "./task.entity";
import { User } from "./user.entity";
import { Point } from "./point.entity";

@Entity()
export class TaskPoint {
    @PrimaryGeneratedColumn()
    id?: number;

    @ManyToOne(type => Point)
    point: Point;

    @ManyToOne(type => User)
    user: User;

    @ManyToOne(type => Task)
    task: Task;

    @Column({ type: "date", nullable: true })
    date?: string;

}