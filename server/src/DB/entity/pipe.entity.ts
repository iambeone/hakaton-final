import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from "typeorm";
import { Owner } from './owner.entity';

enum ConstType {
    Castiron = "Castiron",
    Steel = "Steel",
    Asbestos = "Asbestos",
    ReinforcedConcrete = "ReinforcedConcrete" 
}

@Entity()
export class Pipe {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "varchar", nullable: true })
    title: string;

    @Column({
        type: "enum",
        enum: [
            "water",
            "heat",
            "gas",
            "sewer"
        ], nullable: true
    })
    type?: "water" | "heat" | "gas" | "sewer" | string;

    @Column({ type: "text", nullable: true })
    comment?: string;

    @Column('varchar')
    constType?: ConstType | string

    @Column({ type: 'varchar' })
    constYear?: string;
    
    @Column({ type: 'varchar' })
    depreciation?: string;
    
    @ManyToOne(type => Owner, owner => owner.pipes)
    @JoinColumn()
    owner?: Owner;

    @Column({ type: "json", nullable: true })
    points: any;
}