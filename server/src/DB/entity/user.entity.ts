import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import { Task } from './task.entity';

@Entity()
export class User {    
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: 'text', nullable: true })
    login?: string;

    @Column()
    email: string;

    @Column({ type: 'text', nullable: true })
    name?: string;
        
    @Column({ type: 'text', nullable: true })
    hash?: string;
        
    @Column({ type: 'integer', nullable: true })
    role?: number;

    @OneToMany(type => Task, task => task.user)
    tasks?: Task[];
}
