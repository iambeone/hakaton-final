import { Injectable, CanActivate, ExecutionContext, HttpService, ServiceUnavailableException, UnauthorizedException, ForbiddenException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { decode } from "jsonwebtoken";
import { User } from '../DB/entity/user.entity';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector
    ) { }

    async canActivate(context: ExecutionContext) {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());        
        const authorization: string = context.switchToHttp().getRequest().get('authorization')
        if (!authorization) {
            throw new ForbiddenException('Empty token')
        }
        const token = authorization.replace('Bearer ', '')
        const user = (await decode(token)) as User ;
        const userRoles = roles.map(role => {
            switch(role){
                case 'admin':
                    return 2;
                case 'moderator':
                    return 1;
                case 'user':
                    return 1;
            }
        })
        if(!userRoles.some(role => role == user.role)){
            throw new ForbiddenException('No rights')
        }
        //throw new UnauthorizedException();
        return true;
    }
} 