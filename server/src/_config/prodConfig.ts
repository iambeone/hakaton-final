import { ConfigInterface } from "./config.interface";

export const prodConfig: ConfigInterface = {
    app: {
        port: 4200
    },
    Database: {
        name: 'default',
        type: "postgres",
        host: "188.120.228.225",
        port: 5432,
        username: "hakaton",
        password: "hakaton",
        database: "hakaton",
        entities: ["dist/**/**.entity.js"],
        synchronize: false,
        logging: false,
        subscribers: ["dist/DB/subscribers/**.subscriber.js"],
        migrations: ["./migrations/**/*{.ts,.js}"]
    },

}