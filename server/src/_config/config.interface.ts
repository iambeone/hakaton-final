import { ConnectionOptions } from "typeorm";

export class ConfigInterface {
    app?: {
        port: number
    }
    Database: ConnectionOptions
}