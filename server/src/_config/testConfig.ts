import { ConfigInterface } from "./config.interface";

export const testConfig: ConfigInterface = {
    app: {
        port: 4200
    },
    Database: {
        name: 'default',
        type: "postgres",
        host: "188.120.228.225",
        port: 5432,
        username: "hakaton",
        password: "hakaton",
        database: "hakaton",
        entities: ["src/**/**.entity{.ts,.js}"],
        synchronize: true,
        logging: false,
        subscribers: ["src/DB/subscribers/**.subscriber{.ts,.js}"],
        migrations: ["./migrations/**/*{.ts,.js}"]
    },

}