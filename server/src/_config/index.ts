import { ConfigInterface } from "./config.interface";
import { testConfig } from "./testConfig";
import { prodConfig } from "./prodConfig";

export let config: ConfigInterface;
switch (process.env.NODE_ENV) {

    case 'development':
        config = testConfig;
        break;

    case 'production':
        config = prodConfig;
        break;

    default:
        config = testConfig;
        break;
}