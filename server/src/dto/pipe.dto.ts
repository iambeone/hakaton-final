export namespace PipeDTO {
    export class Payload {
        title?: string
        coords?: [any, any][]
        owner?: number
        type?: string
        comment?: string
        constYear? :string
        depreciation? :string
        constType? :string
    }
}


