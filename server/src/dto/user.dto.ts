export namespace UserDTO {
    export class RequestPayload {
        id?: number
        login?: string
        role?: number
        hash?: string
        password?: string
        password1?: string
        email: string
        name?: string
    }
    export class Payload {
        showAdmin?: string
        showModer?: string
        showUser?: string
    }
}


