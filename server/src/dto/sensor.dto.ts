export namespace SensorDTO {
    export class Payload {
        title: string
        coords?: string
        type?: string
        owner?: number
        max?: number
        min?: number
        value?: number
        risk?: number
    }
}


