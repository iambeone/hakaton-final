export namespace PointTypeDTO {
    export class Payload {
        title: string
        code?: string
        comment?: string
        fields?: string
    }
}


