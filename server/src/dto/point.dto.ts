export namespace PointDTO {
    export class Payload {
        title: string
        coords?: string
        type?: number
        owner?: number
    }
}


