export namespace TaskDTO {
    export class Payload {
        startFrom?: string
        startTill?: string
        endFrom?: string
        endTill?: string
        search?: string
        user?: number
        showFuture?: boolean | string
        showPast?: boolean
        all?: boolean;
    }
    export class MonthPayload{
        month: number
        year: number        
    }
    export class Object{
        startDate?: string
        endDate?: string
        user?: number
        comment?: string
        title: string
    }
}


