export namespace OwnerDTO {
    export class Payload {
        title?: string
        email?: string
        phone?: string
        legal?: string
    }
}


